<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/entrance', [\App\Http\Controllers\testController::class, 'entrance'])->name('entrance');

Route::get('/content', [\App\Http\Controllers\testController::class, 'content'])->name('content');

Route::get('/appendices/{id}', [\App\Http\Controllers\testController::class, 'detail'])->name('detail');
