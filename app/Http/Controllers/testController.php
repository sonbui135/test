<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class testController extends Controller
{
    public function entrance()
    {
        //query muc luc
        $data = [
            'appendice1' => 1,
            'appendice2' => 2,
            'appendice3' => 3,
        ];

        $documentId = 1;
        $documentTemplate = 1;

        return view('temp' . $documentTemplate . '.entrance', compact('documentId'));
    }

    public function content()
    {
        $documentId = 1;
        $documentTemplate = 1;
        $appendices = [1,2,3];
//        $appendices = [1,2,3,4];

        return view('temp' . $documentTemplate . '.content', compact('appendices'));
    }

    public function detail($id)
    {
        $data = $id;
        return view('detail', compact('data'));
    }
}
